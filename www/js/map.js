$wetter = true;																    //Der Wechsel zwischen Wettermodus und Prognosenmodus
$prognose = false;				                                                //Der Wechsel zwischen Wettermodus und Prognosenmodus
$gd_wetter = new Array();														//Assoziativer Array für die Grunddaten vom Wetter
$ma_wetter = {'lon' : 0,
              'lat' : 0,
              'luftF' : '',
              'wolken' : '',
              'windG' : '',
              'windR' : '',
              'sunrise' : 0,
              'sunset' : 0
             };                                                           //Assoziativer Array für "mehr anzeigen" 
$maxZoom = 18;                                                                  //Maximaler Zoom für Baselayers und Overlays    
$minZoom = 2;                                                                   //Minimaler Zoom für Baselayers und Overlays
$myBounds = [[-85.1114157806266, -181.40625],
             [85.0511287798066, 180.70312500000003]];                           //Die Grenze für die Kartenränder
$clickMarker = L.marker();														//Neuer Marker
$layer_BL = L.layerGroup();                                                     //LayerGroup für die vordefinierten Gemeinden für Orthofoto/BL Karte
var timer = 0;                                                                  //Werden für das Timeout zwischen singleClick und dblClick benötigt
var delay = 200;                                                                //Werden für das Timeout zwischen singleClick und dblClick benötigt
var prevent = false;                                                            //Werden für das Timeout zwischen singleClick und dblClick benötigt
var dateSlider = document.getElementById('date-slider');                        //dateSlider Variable
var date;                                                                       //date Variable

if(L.Browser.mobile) {      //Smartphone = keine Legende                                                                                    
    $legendBool = false;
} else {                                   
    $legendBool = true;
}

$myMap = L.map('map', {
    zoomControl: true,                                              //ZoomControl 
    maxBounds: $myBounds,                                           //Kartenrand hinzugefügt
    keyboard: false                                                 //Mittels Keyboard Tasten die KArte bewegen true/false
}).setView([47.48, 7.73], 12);										//Karte wird generiert. Kartenausschnitt bei latlong und Zoom.

var zoomIt = $myMap.getZoom();       //für Funktion 'kartenwechsel'

$tileLayer = L.tileLayer('http://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {			    //Tilelayer von openStreetMap
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>',	//Fusszeile
    noWrap: true,	//Wiederholungen der Karte deaktiviert
    maxZoom: $maxZoom,	     //maxZoom Einstellung
    minZoom: $minZoom		//minZoom Einstellung			
});

$tileLayerBl = L.tileLayer.wms('http://geowms.bl.ch', { //TileLayer vom Geoview Baselland
    layers: 'grundkarte_farbig_group',                  //Grundkarte farbig ausgewählt
    crs: L.CRS.EPSG3857,
    attribution: 'Map data &copy; <a href="https://www.baselland.ch/politik-und-behorden/direktionen/volkswirtschafts-und-gesundheitsdirektion/amt-fur-geoinformation">GIS Basel-Landschaft</a>',	//Fusszeile
    noWrap: true,	//Wiederholungen der Karte deaktiviert
    maxZoom: $maxZoom,	//maxZoom Einstellung
    minZoom: $minZoom		//minZoom Einstellung   
});

$tileLayerOrtho = L.tileLayer.wms('http://geowms.bl.ch', { //TileLayer Orthofoto Baselland
    layers: 'orthofotos_agi_2015_group',
    crs: L.CRS.EPSG3857,
    attribution: 'Map data &copy; <a href="https://www.baselland.ch/politik-und-behorden/direktionen/volkswirtschafts-und-gesundheitsdirektion/amt-fur-geoinformation">GIS Basel-Landschaft</a>',	//Fusszeile
    noWrap: true,	//Wiederholungen der Karte deaktiviert
    maxZoom: $maxZoom,	//maxZoom Einstellung
    minZoom: $minZoom		//minZoom Einstellung   
});

$clouds = L.OWM.clouds({											//OpenWeatherMap- Layer Clouds
	baseURL: 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png',		//?
	showLegend: false,												//Legende anzeigen: Bild muss selbst hinzugefügt werden
	noWrap: true,													//Wiederholungen der Karte deaktiviert
	maxZoom: $maxZoom,													//maxZoom Einstellung
	minZoom: $minZoom,														//minZoom Einstellung
	opacity: 0.5, 													//Deckkraft
    bounds: $myBounds,
	appId: '7ee4d709dbeeebc029df559fddf4e146'});					//API KEY

$temp = L.OWM.temperature({											//OpenWeatherMap- Layer temperatur
	baseURL: 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png',		//?
	showLegend: $legendBool,												//Legende anzeigen: Bild muss selbst hinzugefügt werde
	noWrap: true,													//Wiederholungen der Karte deaktiviert
	maxZoom: $maxZoom,													//maxZoom Einstellung
	minZoom: $minZoom,														//minZoom Einstellung	
	opacity: 0.5, 													//Deckkraft
    bounds: $myBounds,
    legendImagePath: 'res/images/TT.png',
	appId: '7ee4d709dbeeebc029df559fddf4e146'});					//API KEY

$precipitation = L.OWM.precipitation({											//OpenWeatherMap- Layer Niederschlag
	baseURL: 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png',		//?
	showLegend: false,												//Legende anzeigen: Bild muss selbst hinzugefügt werden
	noWrap: true,													//Wiederholungen der Karte deaktiviert
	maxZoom: $maxZoom,													//maxZoom Einstellung
	minZoom: $minZoom,														//minZoom Einstellung	
	opacity: 0.5, 													//Deckkraft
    bounds: $myBounds,
	appId: '7ee4d709dbeeebc029df559fddf4e146'});					//API KEY

$wind = L.OWM.wind({											    //OpenWeatherMap- Layer Wind
	baseURL: 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png',		//?
	showLegend: $legendBool,												//Legende anzeigen: Bild muss selbst hinzugefügt werden
	noWrap: true,													//Wiederholungen der Karte deaktiviert
	maxZoom: $maxZoom,												//maxZoom Einstellung
	minZoom: $minZoom,												//minZoom Einstellung	
	opacity: 0.5, 													//Deckkraft
    bounds: $myBounds,
    legendImagePath: 'res/images/UV.png',
	appId: '7ee4d709dbeeebc029df559fddf4e146'});					//API KEY
	
$city = L.OWM.current({												//TileLayer für das Anzeigen von Städten (früher sogar Stations)
	baseURL: 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png',		//?
	appId: '7ee4d709dbeeebc029df559fddf4e146',						//API Key
	intervall: 15, 													//Alle 15min werden die Daten der Städte updated
	lang: 'de',														//Sprachenausgabe
	minZoom: 5,														//minZoom, in welchem die Städte angezeigtn werden
	showTempMinMax: false,											//TempMinMax nicht anzeigen
	showWindSpeed: false,											//Windstärke nicht anzeigen - !sonstige Optionen: 'speed'|'beaufort'|'both'! true geht nicht
    humidity: false,                                                //Luftfeuchtigkeit anzeigen true, false
    showWindDirection: false,                                       //Windrichtung anzeigen true, false
    pressure: false,                                                //Luftdruck anzeigen true, false
    showTimestamp: true,                                           //Zeitstempel anzeigen true, false
    useLocalTime: false,
	imageLoadingBgUrl: 'res/images/whitebckgrnd.png',				//Weisser HIntergrund beim owmloading.gif
	imageLoadingUrl: 'res/libraries/weathermaplayers/owmloading.gif'});		//Zeigt, dass die Städte geladen werden

$(document).ready(function() {													//Kann erst ausgeführt werden, wenn die Seite (DOM) geladen ist. 

    $tileLayerBl.addTo($myMap);     //Layer BL (Grundkarte farbig) wird beim Start angezeigt
    $baseMaps = { "BL": $tileLayerBl, "Orthofoto": $tileLayerOrtho};		//Grundkarten Layers
    $overlayMaps = {"Bewölkung" : $clouds,"Temperatur" : $temp, "Niederschlag" : $precipitation, "Wind" : $wind, "Gemeinden" : $layer_BL }; //Overlayer
    $layerControl = L.control.layers($baseMaps, $overlayMaps).addTo($myMap);	    //Das Layermenü oben rechts    
    $graphUrl = '';                                                                 //URL für den Graphen. Wird in der onMapClick Funtktion gesetzt
    moment.locale('de');                                                            //Moment.js Formatierungen auf Deutsch gestellt
    
	function onMapClick(e) {														//Funktion für Marker + Popup mit Latlong Angaben bei Klick auf Karte.
        timer = setTimeout(function() {                                             //Ist es ein Singleclick oder ein dblClick? 
            if (!prevent) {
                $latLngArray = splitLatLng(e.latlng.toString());							//Latitude und Longitude Werte im Array
                $wetter_JSON = 'http://api.openweathermap.org/data/2.5/weather?lat='		//API Abruf
                                    + $latLngArray[0] + '&lon='
                                    + $latLngArray[1] 
                                    + '&lang=de&units=metric&appid=7ee4d709dbeeebc029df559fddf4e146&callback';

                $.getJSON($wetter_JSON, function(json) {			//JSON -Get Abfrage
                    
                    $gd_wetter['icon'] = json.weather[0].icon;	//Icon
                    $gd_wetter['temp'] = json.main.temp;		//Temperaturangabe
                    $gd_wetter['stadt'] = json.name;			//Stadt/Bezirksangabe
                    $gd_wetter['zeit'] = json.dt;				//Zeitangabe (Momentan in Unix-Format)
                    $gd_wetter['desc'] = json.weather[0].description;   //Beschreibung (Bsp: bewölkt)

                    if($wetter == true && $prognose == false) {			//Booleanwerte müssen stimmen

                        $ma_wetter['lon'] = json.coord.lon;			//Longitude Wert
                        $ma_wetter['lat'] = json.coord.lat;			//Latitude Wert
                        $ma_wetter['luftF'] = json.main.humidity;	//Luftfeuchtigkeit
                        $ma_wetter['wolken'] = json.clouds.all;		//Bewölkung
                        $ma_wetter['windG'] = benutzerfreundlichWindG(json.wind.speed);      //Windgeschwindigkeit (Benutzerfreundlich)
                        $ma_wetter['windR'] = benutzerfreundlichWindR(json.wind.deg);       //windrichtung (Benutzerfreundlich)
                        $ma_wetter['sunrise'] = json.sys.sunrise;   //Sonnenaufgang
                        $ma_wetter['sunset'] = json.sys.sunset;     //Sonnenuntergang

                        $clickMarker.setLatLng(e.latlng).addTo($myMap);		//Latlong des Markers bei Klick
                        setMarker();	       //Setzt den Marker *** Muss hier sein, ansonsten zeigt die Abfrage die Anzeige vom Klick vorher ***
                    } else if($wetter == false && $prognose == true) {		//Beide Angaben müssen stimmen
                        
                        $clickMarker.setLatLng(e.latlng).addTo($myMap);		//Latlong des Markers bei Klick
                        $graphUrl = 'http://api.openweathermap.org/data/2.5/forecast?lat='
                                    + $latLngArray[0] + '&lon='
                                    + $latLngArray[1] + '&lang=de&units=metric&appid=7ee4d709dbeeebc029df559fddf4e146&callback';
                        setMarker();	//Setzt den Marker                
                    } else {
                        alert("Es ist ein Fehler aufgetreten. Bitte Laden Sie den Service erneut.");			//Fehlermeldung
                        console.log("Boolean Wetter: " + $wetter);												//Konsolenmeldung
                        console.log("Boolean Prognose: " + $prognose);
                    }
                });            
            }
            
        prevent = false;
        }, delay);
	}
	$myMap.on('click', onMapClick);												//Funktion wird bei onClick ausgeführt
    $myMap.on("dblclick", function() {                                          
        clearTimeout(timer);
        prevent = true;
    });                                  //Timeout zum unterscheiden zwischen dblClick und singleClick
    $myMap.on('locationerror', onLocationError);                                //Funktion wird bei locationerror ausgeführt
    $myMap.on('locationfound', onLocationFound);                                //...locationFound...
    $myMap.on('dragend', dragEnd);                                              //...dragend...
    $myMap.on('zoomend', zoomEnd);                                              //...zoomend...
    $myMap.on('popupopen', function(){                                    
        if($wetter == true && $prognose == false) {
            $.each($ma_wetter, function(key, value){                                //Und dementsprechend wird der Inhalt angezeigt.
                if($('#' + key).is(':checked')) {
                    $('.' + key + 'A').show();
                } else {
                    $('.' + key + 'A').hide();
                }
            });            
        } else if($wetter == false && $prognose == true) {                      //setMarker Funktion wird aufgerufen, damit der Graph erneut gezeichnet wird
            date = moment().format('L');                                        //Aktuelles Datum (Graph)
            slider = $('.my-slider').unslider();                                //Slider
            slider.on('unslider.change', function(event, index, slide) {	    //Bei Change wird date an das aktuell angezeigte Datum angepasst	
                date = $('.unslider-active').text().substring(0, 10);
                graph($graphUrl);											    //Der Graph wird neu gezeichnet
            });
            graph($graphUrl);
        }
    });                            //Wenn ein Pop-Up geöffnet wird, wird gecheckt welche Checkboxen checked sind
    
    $('#CarouselModi').bcSwipe({ threshold: 20 });                  //Funktion, damit das Carousel auf dem Mobile Phone per Swipe verändert werden kann
    $('#startseite').click(function(){                                          
         window.location.href = 'index.html';   
    });                      //Startseite Button Funktion -> Zurück zur Startseite
    $('body').keyup(function(e) {                                   
        if (e.which == '27') {
            $('#oModal').modal('toggle');
        }
    });                           //Esc-Key wird festgestellt -> Das Optionsmenü öffnet sich
    $('.prognose').click(function(){                                
        $wetter = false;                                            //Die Booleanwerte werden so verändert, dass nun die Prognose angezeigt wird
        $prognose = true;
        $("input.check").prop("disabled", true);                    //Im Prognosenmodus werden die Checkboxen disabled
        if($myMap.hasLayer($tileLayerBl) === true || $myMap.hasLayer($tileLayerOrtho) === true) {   //dateSlider gibt es nur bei den BL-Layer
            dateSliderF();                                               //dateSlider() - Funktion für den 3. Modus, mit welchem man den forecast durchsliden kann
        }
        $('#oModal').modal('hide');                                 //Optionsmenü wird ebenfalls geschlossen
    });                        //Bei Click auf das Prognose Icon im Optionsmenü 
    $('.wetter').click(function(){                                  
        $prognose = false;                                          //Die Booleanwerte werden so verändert, dass nun das aktuelle Wetter angezeigt wird
        $wetter = true;
        $("input.check").prop("disabled", false);                   //Checkboxen werden enabled
        $('#oModal').modal('hide');
        if($myMap.hasLayer($tileLayerBl) === true || $myMap.hasLayer($tileLayerOrtho) === true) {   //Vordefinierte Gemeinden nur bei Layer BL
            gemeindenOverlay();
            dateSlider.noUiSlider.destroy();
        }
    });                          //Bei Click auf das aktuelle Wetter Icon im Optionsmenü
    $('.check').click(function(){                                   
        if($('#' + this.id).is(':checked')) {
            $('.' + this.id + 'A').show();
        } else {
            $('.' + this.id + 'A').hide();
        }         
    });                           //Checkboxen: Bei Click Show oder Hide den anzuzeigenden Content
    
    L.Control.Options = L.Control.extend({
        options: {
            position: 'topright',
            optionsText: '<span class="glyphicon glyphicon-cog mobile" style="line-height: 1.5;font-size: larger;"></span>', //CSS wird hier geschrieben, da es sonst von Bootstrap üeberschrieben wird.
            optionsTitle: 'Optionen'
        }, 
        onAdd: function (map) {
            
            var controlName = 'Optionen',
                container = L.DomUtil.create('div', controlName + ' leaflet-bar'),
                options = this.options;
            
            this._optionsButton = this._createButton(options.optionsText, options.optionsTitle,
            controlName + '-home', container, this._options);            
            return container;
        },
        
        onRemove: function(map) {
            // Nothing to do here
        },        
        
        _options: function (e) {                    //Öffnet das Funktionsmenü
            $('#oModal').modal('toggle');
        },

        _createButton: function (html, title, className, container, fn) {               //ErstelltButton Funktion
            var link = L.DomUtil.create('a', className, container);
            link.innerHTML = html;
            link.href = '#';
            link.title = title;
            if(L.Browser.mobile) {
                link.style.width = '44px';
                link.style.height = '44px';
            } else {
                link.style.width = '36px';
                link.style.height = '36px';    
            }

            //link.style.backgroundPosition = 'center';

            L.DomEvent.on(link, 'mousedown dblclick', L.DomEvent.stopPropagation)
                .on(link, 'click', L.DomEvent.stop)
                .on(link, 'click', fn, this)
                .on(link, 'click', this._refocusOnMap, this);

            return link;
        }        
    });                   //Leaflet OptionenButton
    L.Control.Locate = L.Control.extend({
        options: {
            position: 'topright',
            optionsText: '<span class="fa fa-map-marker mobile" aria-hidden="true" style="line-height: 1.5;font-size: larger;"></span>', //CSS wird hier geschrieben, da es sonst von Bootstrap üeberschrieben wird.
            optionsTitle: 'Lokalisierung'
        }, 
        onAdd: function (map) {

            var controlName = 'Locate',
                container = L.DomUtil.create('div', controlName + ' leaflet-bar'),
                options = this.options;
            
            this._locateButton = this._createButton(options.optionsText, options.optionsTitle,
            controlName + '-home', container, this._locate);            
            return container;
        },
        
        onRemove: function(map) {
            // Nothing to do here
        },        
        
        _locate: function (e) {                    //Öffnet das Funktionsmenü
            $myMap.locate({setView: true, maxZoom: 16});
        },

        _createButton: function (html, title, className, container, fn) {               //ErstelltButton Funktion
            var link = L.DomUtil.create('a', className, container);
            link.innerHTML = html;
            link.href = '#';
            link.title = title;
            if(L.Browser.mobile) {
                link.style.width = '44px';
                link.style.height = '44px';
            } else {
                link.style.width = '36px';
                link.style.height = '36px';    
            }

            //link.style.backgroundPosition = 'center';

            L.DomEvent.on(link, 'mousedown dblclick', L.DomEvent.stopPropagation)
                .on(link, 'click', L.DomEvent.stop)
                .on(link, 'click', fn, this)
                .on(link, 'click', this._refocusOnMap, this);

            return link;
        }        
    });                    //Leaflet LocateMe Button
    
    var optionen = new L.Control.Options();
    var locate = new L.Control.Locate();               
    optionen.addTo($myMap);                            //Hinzufügen des neuen OptionenButtons
    locate.addTo($myMap);                              //Hinzufügen des LocateMe Button

    if(L.Browser.mobile) {
        $('.mobile').css("line-height", 1.9);
        $('.mobile').css("font-size", "x-larger");
    }                       //Einstellungen für die Buttons auf dem Smartphone        
    
    $clouds.setZIndex(1000);                           //Z-Index wird manuell gesetzt, da es sonst überdeckt wird
    $temp.setZIndex(1000);
    $precipitation.setZIndex(1000);
    $wind.setZIndex(1000);

    gemeindenOverlay(function() {
        $('.leaflet-control-layers-overlays').children().last().before('<div class="leaflet-control-layers-separator"></div>');
        
    });
    
});

function gemeindenOverlay(callback) {

    $layer_BL.clearLayers();
    $gemeindenUrlArray = [];
    var counter = 0;
    if($myMap.hasLayer($tileLayerBl) === true || $myMap.hasLayer($tileLayerOrtho) === true) {
        
        $jsonPfad = "res/Gemeinden.json";                                          //JSON-Datei mit den Gemeinden
        $.getJSON($jsonPfad, function(json){
            for($i=0;$i<json.Gemeinden.length;$i++) {
                $gemeindenUrlArray[$i] = "http://api.openweathermap.org/data/2.5/weather?lat=" + json.Gemeinden[$i].Lat + "&lon=" + json.Gemeinden[$i].Long  + "&lang=de&units=metric&appid=7ee4d709dbeeebc029df559fddf4e146&callback";
            }
            
            vordefinierteGemeinden();
        });
        
        function vordefinierteGemeinden() {
            for(var i=0;i<$gemeindenUrlArray.length;i++) {
                $.getJSON($gemeindenUrlArray[i], function(json){                 //JSON Abfrage - Die benötigten Infos werden im Array gespeichert

                    $ausgabeWetter = ausgabeWetter(json.name, json.weather[0].description, json.weather[0].icon, json.main.temp, json.coord.lon, json.coord.lat,
                                  json.main.humidity, json.clouds.all, benutzerfreundlichWindG(json.wind.speed), benutzerfreundlichWindR(json.wind.deg),
                                  json.sys.sunrise, json.sys.sunset, json.dt);

                    gemeindeLayer(json.coord.lat, json.coord.lon, json.weather[0].icon, json.main.temp);
                    counter++;
                    if(counter == $gemeindenUrlArray.length) {
                        if (callback && typeof(callback) === "function") {
                            callback();
                        }                        
                    }
                });                
            }
        }
    }
}
function splitLatLng($latlng) {													//Funktion zur Teilung des Strings in Latitude und Longitude
	$latLngFull = $latlng.slice($latlng.indexOf('(') +1,$latlng.indexOf(')'));	//Latitude,Longitude wird aus dem String mittels Slice und indexOf() ausgeschnitten
	$latLngArray = $latLngFull.split(',');										//Split in Latitude und Longitude Array
	for($i = 0; $i < $latLngArray.length; $i++) {							
		$latLngArray[$i] = $.trim($latLngArray[$i]);							//White Spaces werden mit trim entfernt
	}
	return $latLngArray;
}
function setMarker() {																//Funktion, welche die Marker setzt
	if($wetter == true && $prognose == false) {											//Booleanwerte müssen stimmen
        $ausgabeWetter = ausgabeWetter($gd_wetter['stadt'], $gd_wetter['desc'], $gd_wetter['icon'], $gd_wetter['temp'], 
                                       $ma_wetter['lon'], $ma_wetter['lat'], $ma_wetter['luftF'], $ma_wetter['wolken'],
                                       $ma_wetter['windG'], $ma_wetter['windR'], $ma_wetter['sunrise'], $ma_wetter['sunset'], $gd_wetter['zeit']);

	} else if($wetter == false && $prognose == true) {
        $ausgabeWetter = '<div id="chartContainer" style="height: 300px; width: 300px; position: relative"></div>' +
                        '<div class="my-slider">' + 
                            '<ul><li>'+ moment().format('L') +'</li>'	//Unslider.js Slider zur Veränderung des Datums für die Prognose
                             +' <li>' + moment(new Date((new Date()).valueOf() + 1000*3600*24)).format('L')  +'</li>'
                             +' <li>' + moment(new Date((new Date()).valueOf() + 2000*3600*24)).format('L')  +'</li>' 
                             +' <li>' + moment(new Date((new Date()).valueOf() + 3000*3600*24)).format('L')  +'</li>'
                             +' <li>' + moment(new Date((new Date()).valueOf() + 4000*3600*24)).format('L')  +'</li></ul>' +                        
                        '</div>' /*+ 
                        '<div id="censored"></div>'*/;
	} else {
		alert("Es ist ein Fehler aufgetreten. Bitte Laden Sie den Service erneut.");			//Fehlermeldung
		console.log("Boolean Wetter: " + $wetter);												//Konsolenmeldung
		console.log("Boolean Prognose: " + $prognose);
	}
    $clickMarker.bindPopup($ausgabeWetter).openPopup();								//PopUp mit LatLong Angaben dem Marker hinzugefügt
}
function graph($url) {											//Funktion Graph mit Parameter URL von onClick
	var dataPoints = [];										//Die Temperaturpunkte werden diesem Array hinzugefügt
	var url = $url;
	$.getJSON(url, function(json) {									//Get Json Daten von der oben angegebenen 'url'
		for(var i = 0; i < json.list.length; i++) {	
			if(moment.unix(json.list[i].dt).format('L') == date) {	//Die Daten vom JSON werden mit dem aktuell anzuzeigendem Datum verglichen
                var temperatur = parseInt((Math.round(json.list[i].main.temp * 100)/100).toFixed(1));
				dataPoints.push({label: moment.unix(json.list[i].dt).format('LLL').substring(18), y: temperatur, name: json.list[i].weather[0].icon}); //Es werden nur die gleichen Daten hinzugefügt. (Zeit, Temperatur, Icon werden gespeichert)
			}
		}		
	
		var chart = new CanvasJS.Chart("chartContainer",{       // Der CanvasJS-Graph 
			title:{												// Titel des Graphen
				text: $gd_wetter['stadt'],              			
			}, 
			axisY: {											//Y-Achse
				includeZero: true,								//0-Wert in den Graphen einbeziehen
				suffix: "°C",									//Erscheint hinter jedem Wert : 40°C
				minimum: -20,									//Minimum-Wert des Graphen
				maximum: 50,									//Maximum-Wert des Graphen
				gridThickness: 1								//Dicke der Y-Linien
			},
			toolTip:{											//Wert beim darüberfahren
				shared: true,									//Wird angezeigt?
				content: "<strong>Zeit: </strong> {label} </br>" 
						+"<strong>Temperatur: </strong> {y}°C",	//Wert, welcher angezeigt wird. {Zeit} & {y} stammen von data[0].dataPoints[i]
			},
			data: [												//Datenarray
			{
				type: "line",									//Art des Graphen
				color: "red",									//Farbe der Linie		
				dataPoints: dataPoints
			}]
		});
		chart.render();											//Der Graph wird gerendert 
		
		
		var images = [];
		
		addImages(chart);										//addImages Funktionaufruf

		function addImages(chart){								//Funktion addImages mit Parameter 'chart'
			for(var i = 0; i < chart.data[0].dataPoints.length; i++){
				var icon = chart.data[0].dataPoints[i].name;	//Die Icon Namen 
				images.push($('<img>').attr('src', "http://openweathermap.org/img/w/" + icon + ".png", 'alt', "wettericon"));	//Icons werden dem Images Array hinzugefügt
				images[i].attr('class', icon).appendTo($("#chartContainer>.canvasjs-chart-container"));	//Images werden dem HTML chartContainer hinzugefügt und somit angezeigt
				positionImage(images[i], i);	//Funktionsaufruf positionImage zur Positionierung und Einstellung der Breite
			}
		} 
		function positionImage(image, index){	//Funktion positionImage
			var imageCenter = chart.axisX[0].convertValueToPixel(chart.data[0].dataPoints[index].x);	//ImageCenter wird auf die x-Seite Center gestellt
			var imageTop =  chart.axisY[0].convertValueToPixel(chart.axisY[0].maximum);					//Wird auf die maximale Höhe gesetzt

			image.width('40px').css({ "left": imageCenter - 20 + "px",									//CSS für das Bild
									"position": "absolute","top":imageTop + "px",
									"position": "absolute"});
		}
	});		
}
function dateSliderF() {                                                //DateSlider Funktion - Hier wird alles für den 3. Modus erledigt.
    $timestamps = [];                                                   //TimeArray mit den Zeiten, welche für den Slider benötigt werden
    $mod3Array = [];                                                    //Beinhaltet die Daten für die verschiedenen Zeiten
    var gemeindeJSON = 'res/Gemeinden.json';                            //Pfad zur JSON Datei mit den vordefinierten Gemeinden                                       
    var url = 'http://api.openweathermap.org/data/2.5/forecast?lat=47.56&lon=7.59&lang=de&units=metric&appid=7ee4d709dbeeebc029df559fddf4e146&callback'; 
    var stadt;
    var lon;
    var lat;
    var urlArray = [];
    var nameArray = [];
    var latArray = [];
    var longArray = [];
    
    //eine Einmalige Abfrage für die Zeiten für PrognoseSlider
    $.getJSON(url, function(json){                      //AJAX-Abfrage
        for($i=0;$i<json.list.length;$i++) {        
            $timestamps.push(json.list[$i].dt + 7200);     //Fügt die Timestamps von openweathermap dem timestamps Array hinzu (+2h für unsere Zeit)
        }
        getDataLocalJson();                         
    });
    
    function getDataLocalJson() {                       //Funktion, mit welcher die Daten zur Ausführung der lokalen JSON-datei verarbeitet werden
        $.getJSON(gemeindeJSON, function(json) {            //AJAX-Abfrage
            
            for($i=0;$i<json.Gemeinden.length;$i++) {   //For-Loop (Anzahl Gemeinden in 'Gemeinden.json')
                
                urlArray[$i] = ('http://api.openweathermap.org/data/2.5/forecast?lat='+ 
                                json.Gemeinden[$i].Lat +'&lon='+ 
                                json.Gemeinden[$i].Long +'&lang=de&units=metric&appid=7ee4d709dbeeebc029df559fddf4e146&callback'); //Wird dem urlArray hinzugefügt
                nameArray[$i] = json.Gemeinden[$i].Name;    //Wird dem nameArray hinzugefügt.
                latArray[$i] = json.Gemeinden[$i].Lat;      //Wird dem latArray hinzugefügt.
                longArray[$i] = json.Gemeinden[$i].Long;    //Wird dem longArray hinzugefügt
            }
            
            if(urlArray.length == json.Gemeinden.length && 
               nameArray.length == json.Gemeinden.length && 
               latArray.length == json.Gemeinden.length && 
               longArray.length == json.Gemeinden.length) {                        //Prüft, ob alle benötigten Arrays die gleiche Länge haben wie das JSON-Dokument
                for($i=0;$i<urlArray.length;$i++) {         //For-Loop mit Anzahl an Einträgen in der urlArray(nameArray/latArray/longArray würde auch gehen)
                    getInfosMod3(urlArray[$i], nameArray[$i], latArray[$i], longArray[$i]);     //Führt die Funktion getInfosMod3 aus.
                }
            }
        });
    }
    
    function getInfosMod3(url, stadt, lat, lon) {           //Funktion 'getInfosMod3' mit Parameter 'url', 'stadt' 'lat' und 'long'
        $.getJSON(url, function(json) {                         //AJAX-Abfrage
            
            for($i=0;$i<json.list.length;$i++) {            //For-Loop über alle JSON-Listen Einträgen
                
                $mod3Array[$mod3Array.length] = {           //Die benötigten Daten werden im $mod3Array abgelegt
                    'icon': json.list[$i].weather[0].icon,
                    'stadt' : stadt,
                    'desc' : json.list[$i].weather[0].description,
                    'temp' : json.list[$i].main.temp,
                    'tstmp' : json.list[$i].dt,
                    'lon' : lon,
                    'lat' : lat };
                
                if($mod3Array.length == json.list.length * urlArray.length) {   //Prüfung, ob alle For-Loops fertig sind
                    createSlider();                                             //Damit der Slider endlich erstellt werden kann
                }
            }
        });
    }
    
    function createSlider() {
        noUiSlider.create(dateSlider, {                         //Erstellt den Datumsslider
            start: [$timestamps[0]*1000],                       //Startwert
            step: 3 * 60 * 60 * 1000,                           //Step (die Rechnung in Millisekunde für den Schritt von 3 Stunden)
            tooltips: true,                                     //Mit den Tooltips wird das Datum + Zeit angezeigt
            animate: true,
            animationDuration: 300,            
            range: {                                
                'min': $timestamps[0]*1000,                     //MIn
                'max': $timestamps[$timestamps.length-1]*1000   //und Max wert des Sliders
            },
            format: {                                           //Die Werte für die Tooltips
                to: function (value) {             
                    moment.locale("de");
                    return moment.unix(value / 1000 - 7200).format("LLLL");
                },
                from: function (value) {
                    return value;
                }
            }           
        });
        
        dateSlider.noUiSlider.on('update', function() {         //Slider Update Funktion - (Wird auch beim erstellen des Sliders schon aufgerufen)
            var time = moment(dateSlider.noUiSlider.get(), "LLLL").unix();  //Vom Slider wird leider die formatierte Zeitangabe genoommen (-2h zeitunterschied)
            $layer_BL.clearLayers();                                            //Layer wird 'gecleart', damit die Bilder nicht überlappen
            for($i=0;$i<$mod3Array.length;$i++) {                               //For-Loop mit den Einträgen aus Mod3Array
                
                if($mod3Array[$i].tstmp == time) {            //Falls die Zeitangabe aus dem Array mit dem obigen übereinstimmt
                    $ausgabeWetter = ausgabeWetter($mod3Array[$i].stadt,
                                                   $mod3Array[$i].desc,
                                                   $mod3Array[$i].icon,
                                                   $mod3Array[$i].temp,
                                                   $mod3Array[$i].lon,
                                                   $mod3Array[$i].lat,
                                                   null,
                                                   null,
                                                   null,
                                                   null,
                                                   null,
                                                   null,
                                                   $mod3Array[$i].tstmp);
                    gemeindeLayer($mod3Array[$i].lat, $mod3Array[$i].lon, $mod3Array[$i].icon, $mod3Array[$i].temp);
                }
            }
        });        
    }
}
function onLocationFound(e) {
    var radius = e.accuracy / 2;
    L.marker(e.latlng).addTo($myMap)
        .bindPopup("Sie befinden sich in einem Umkreis von " + radius + " Metern.").openPopup();
    L.circle(e.latlng, radius).addTo($myMap);
}
function onLocationError(e) {
    alert("Sie konnten nicht lokalisiert werden.");
}
function dragEnd(e) {
    kartenwechsel();
}
function zoomEnd(e) {
    zoomIt = $myMap.getZoom();
    kartenwechsel();
}
function kartenwechsel() {
    var checkBounds = $myMap.getCenter();
    if(zoomIt >= 12 && checkBounds.lat > 47.33 && checkBounds.lat < 47.59 && checkBounds.lng < 7.96 && checkBounds.lng > 7.33) {
        
        $myMap.removeLayer($tileLayer);
        $layerControl.removeLayer($tileLayer);
        $layerControl.addOverlay($layer_BL, "Gemeinden");
        $layerControl.addBaseLayer($tileLayerOrtho, "Orthofoto");
        $layerControl.addBaseLayer($tileLayerBl, "BL");
        $myMap.addLayer($tileLayerBl);        
        
        if($myMap.hasLayer($city)) {
            $myMap.addLayer($layer_BL);
        }
        $myMap.removeLayer($city);
        $layerControl.removeLayer($city);


        if($wetter === false && $prognose === true) {
            dateSliderF();
        }
        
    } else {
        $myMap.removeLayer($tileLayerOrtho);
        $myMap.removeLayer($tileLayerBl);
        if($myMap.hasLayer($layer_BL)) {
            $myMap.addLayer($city);
        }
        $myMap.removeLayer($layer_BL);
        $layerControl.removeLayer($tileLayerOrtho);
        $layerControl.removeLayer($tileLayerBl);
        $layerControl.removeLayer($layer_BL);

        $layerControl.addOverlay($city, "Städte");
        
        
        $layerControl.addBaseLayer($tileLayer, "Welt");
        $myMap.addLayer($tileLayer);
        if($wetter === false && $prognose === true) {
            if(typeof dateSlider.noUiSlider !== 'undefined') {
                dateSlider.noUiSlider.destroy();
            }
        }
    }
    $('.leaflet-control-layers-overlays').children().last().before('<div class="leaflet-control-layers-separator"></div>');
}
function benutzerfreundlichWindG(windG) {
    switch (true) {
    case (0.0 < windG && windG < 0.3 ):
        return 'Windstille';
        break;
    case (0.3 < windG && windG < 1.6 ):
        return 'Leichter Zug';
        break;
    case (1.6 < windG && windG < 3.4 ):
        return 'Leichte Brise';
        break;
    case (3.4 < windG && windG < 5.5 ):
        return 'Schwache Brise';
        break;
    case (5.5 < windG && windG < 8.0 ):
        return 'Mässige Brise';
        break;
    case (8.0 < windG && windG < 10.8 ):
        return 'Frische Brise';
        break;
    case (10.8 < windG && windG < 13.9 ):
        return 'Starker Wind';
        break;
    case (13.9 < windG && windG < 17.2 ):
        return 'Steifer Wind';
        break;
    case (17.2 < windG && windG < 20.8 ):
        return 'Stürmischer Wind';
        break;
    case (20.8 < windG && windG < 24.5 ):
        return 'Sturm';
        break;
    case (24.5 < windG && windG < 28.5 ):
        return 'Schwerer Sturm';
        break;
    case (28.5 < windG && windG < 32.7 ):
        return 'Orkanartiger Sturm';
        break;
    case (32.7 < windG):
        return 'Orkan';
        break;
    }
}
function benutzerfreundlichWindR(windR) {
    switch (true) {
    case (348.75 < windR < 11.25):
        return 'N';
        break;
    case (11.25 < windR < 33.75):
        return 'NNE';
        break;
    case (33.75 < windR < 56.25):
        return 'NE';
        break;
    case (56.25 < windR < 78.75):
        return 'ENE';
        break;
    case (78.75 < windR < 101.25):
        return 'E';
        break;
    case (101.25 < windR < 123.75):
        return 'ESE';
        break;
    case (123.75 < windR < 146.25):
        return 'SE';
        break;
    case (146.25 < windR < 168.75):
        return 'SSE';
        break;
    case (168.75 < windR < 191.25):
        return 'S';
        break;
    case (191.25 < windR < 213.75):
        return 'SSW';
        break;
    case (213.75 < windR < 236.25):
        return 'SW';
        break;
    case (236.25 < windR < 258.75):
        return 'WSW';
        break;
    case (258.75 < windR < 281.25):
        return 'W';
        break;
    case (281.25 < windR < 303.75):
        return 'WNW';
        break;  
    case (303.75 < windR < 326.25):
        return 'NW';
        break;
    case (326.25 < windR < 348.75):
        return 'NNW';
        break;                                
    }    
}
function ausgabeWetter(stadt, desc, icon, temp, lon, lat, luftF, wolken, windG, windR, sunrise, sunset, tstmp) {
    return '<div class="owm-popup-name">' + stadt + '</div>' +              //Die Ausgabe für das PopUp
             '<div class="owm-popup-description">' + desc + '</div>' + 
             '<div class="owm-popup-main">' +
                '<img alt="icon" src="http://openweathermap.org/img/w/' + icon + '.png"/>' + 
                '<span class="owm-popup-temp">' + (Math.round(temp * 100)/100).toFixed(1) + '°C</span>' +
                '<div class="lonA" style="display:none">' +                                                            //'Mehr anzeigen' Optionen
                    '<div class="ma">Longitude: </div><div class="ma_info">' + lon + '°</div>' +      //Werden versteckt
                '</div>' +                                                                                          //dem Popup hinzugefügt
                '<div class="latA" style="display:none">' + 
                    '<div class="ma">Latitude: </div><div class="ma_info">' + lat + '°</div>' +       //Per Menü sichtbar machen möglich
                '</div>' +
                '<div class="luftFA" style="display:none">' + 
                    '<div class="ma">Luftfeuchtigkeit: </div><div class="ma_info">' + luftF + '%</div>' +
                '</div>' +
                '<div class="wolkenA" style="display:none">' + 
                    '<div class="ma">Bewölkung: </div><div class="ma_info">' + wolken + '%</div>' + 
                '</div>' +
                '<div class="windGA" style="display:none">' + 
                    '<div class="ma">Windgeschwindigkeit: </div><div class="ma_info">' + windG + '</div>' +     //class ma_infos -> css 
                '</div>' +
                '<div class="windRA" style="display:none">' + 
                    '<div class="ma">Windrichtung: </div><div class="ma_info">' + windR + '</div>' + 
                '</div>' +
                '<div class="sunriseA" style="display:none">' + 
                    '<div class="ma">Sonnenaufgang: </div><div class="ma_info">' + new Date(sunrise*1000).toLocaleTimeString() + '</div>' + 
                '</div>' +
                '<div class="sunsetA" style="display:none">' + 
                    '<div class="ma">Sonnenuntergang: </div><div class="ma_info">' + new Date(sunset*1000).toLocaleTimeString() + '</div>' + 
                '</div>' + 
             '</div>' + 
             '<div class="owm-popup-timestamp">' + moment.unix(tstmp).format('LLLL') + '</div>';        
}
function gemeindeLayer(lat, lon, icon, temp) {
    $layer_BL.addLayer(L.marker([lat,lon], {    //Marker wird $layer-BL hinzugefügt - Marker bekommt Lat/lon aus Array 
        icon: L.divIcon({className: "",
                         iconUrl: '',
                         iconSize: [50, 50],
                         iconAnchor: [25, 25],
                         popupAnchor: [0, -10],
                         html:'<div class="owm-icondiv"><img src="http://openweathermap.org/img/w/' + 
                            icon + '.png">'+
                            '<div class="owm-icondiv-temp">'+ (Math.round(temp * 100)/100).toFixed(1) +'°C</div></div>'})
    }).bindPopup($ausgabeWetter)).addTo($myMap);            //divIcon mit dazugehörigem HTML für das Aussehen und Informationen 
}