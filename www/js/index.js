$(document).ready(function() {
    $('.bl').click(function() {
        window.location.href = 'map.html';
    });
    
    $wetter_JSON = 'http://api.openweathermap.org/data/2.5/weather?lat=47.48&lon=7.74&lang=de&units=metric&appid=7ee4d709dbeeebc029df559fddf4e146&callback';

    $.get($wetter_JSON, function(json) {			//JSON -Get Abfrage
        $icon = json.weather[0].icon;	          //Icon
        $('#temp').html((Math.round(json.main.temp*100)/100).toFixed(1) + ' °C');		//Temperaturangabe
        $('#icon').attr('src', 'http://openweathermap.org/img/w/' + $icon + '.png');
    });
});